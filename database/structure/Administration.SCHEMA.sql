DECLARE @name VARCHAR(100) = 'Administration'

IF NOT EXISTS
(
  SELECT 1
  FROM sys.schemas
  WHERE name = @name
)
  EXEC('CREATE SCHEMA ' + @name)

/*-----[ Test ]------------------------------
SELECT schema_id, name
FROM sys.schemas
WHERE name = 'Administration'
-------------------------------------------*/

/*-----[ Down ]------------------------------
DROP SCHEMA Administration
-------------------------------------------*/
