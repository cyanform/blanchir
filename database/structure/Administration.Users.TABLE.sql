DECLARE @schema VARCHAR(100) = 'Administration'

-- sysobjects.xtype descriptions:
-- SELECT name FROM master..spt_values WHERE type = 'O9T'
IF NOT EXISTS
(
  SELECT 1
  FROM sys.sysobjects
  WHERE xtype = 'U' AND name = 'Users'
)
  CREATE TABLE Administration.Users
  (
    UserID BIGINT IDENTITY,
    UserName VARCHAR(100) NOT NULL,
    CONSTRAINT PK_Users PRIMARY KEY (UserID)
  )

/*-----[ Test ]------------------------------
SELECT xtype, name
FROM sys.sysobjects
WHERE name = 'Users'
-------------------------------------------*/

/*-----[ Down ]------------------------------
DROP TABLE Administration.Users
-------------------------------------------*/
