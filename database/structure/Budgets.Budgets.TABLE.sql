DECLARE @schema VARCHAR(100) = 'Budgets'

-- sysobjects.xtype descriptions:
-- SELECT name FROM master..spt_values WHERE type = 'O9T'
IF NOT EXISTS
(
  SELECT 1
  FROM sys.sysobjects
  WHERE xtype = 'U' AND name = 'Budgets'
)
  CREATE TABLE Budgets.Budgets
  (
    BudgetID BIGINT IDENTITY,
    UserID BIGINT NOT NULL,
    BudgetName VARCHAR(100) NOT NULL,
    CONSTRAINT PK_Budgets PRIMARY KEY (BudgetID),
    CONSTRAINT FK_Budgets_Users FOREIGN KEY (UserID)
    REFERENCES Administration.Users (UserID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
  )

/*-----[ Test ]------------------------------
SELECT xtype, name
FROM sys.sysobjects
WHERE name = 'Budgets'
-------------------------------------------*/

/*-----[ Down ]------------------------------
DROP TABLE Budgets.Budgets
-------------------------------------------*/
