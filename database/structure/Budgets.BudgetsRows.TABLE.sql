DECLARE @schema VARCHAR(100) = 'Budgets'

-- sysobjects.xtype descriptions:
-- SELECT name FROM master..spt_values WHERE type = 'O9T'
IF NOT EXISTS
(
  SELECT 1
  FROM sys.sysobjects
  WHERE xtype = 'U' AND name = 'BudgetsRows'
)
  CREATE TABLE Budgets.BudgetsRows
  (
    BudgetRowID BIGINT IDENTITY,
    BudgetID BIGINT NOT NULL,
    Completed BIT NOT NULL,
    BudgetRowName VARCHAR(100) NOT NULL,
    CONSTRAINT PK_BudgetsRows PRIMARY KEY (BudgetRowID),
    CONSTRAINT FK_BudgetsRows_Budgets FOREIGN KEY (BudgetID)
    REFERENCES Budgets.Budgets (BudgetID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
  )

/*-----[ Test ]------------------------------
SELECT xtype, name
FROM sys.sysobjects
WHERE name = 'BudgetsRows'
-------------------------------------------*/

/*-----[ Down ]------------------------------
DROP TABLE Budgets.BudgetsRows
-------------------------------------------*/
