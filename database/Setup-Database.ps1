<# In case you obtained the next error running this scenario
       The term '.\Setup-Database\New-Database.ps1' is not recognized as the name of a cmdlet, function...
   set location (working directory) to the parent directory of this file #>

param(
    [Parameter(Mandatory=$False)]
    [bool]
    $preserveParams = $False
)

.\Setup-Database\New-Database.ps1 -preserveParams $preserveParams;
.\Setup-Database\Call-NewServerFirewallRule.ps1;
.\Setup-Database\New-Schemas.ps1;
