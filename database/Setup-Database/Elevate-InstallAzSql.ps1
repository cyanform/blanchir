﻿# install az.sql module
Write-Host "Elevating permissions to install az.sql"
$script = ([System.IO.Path]::Combine($PSScriptRoot, "Install-AzSql.ps1"));
Start-Process powershell -ArgumentList $script -Verb RunAs -Wait;
