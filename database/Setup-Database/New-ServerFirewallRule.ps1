Function Import-AzSql
{
    if((Get-Module | Where-Object -Property Name -EQ "AzureRM.Profile").Count > 0)
    {
        Remove-Module AzureRM.Profile
    }

    Import-Module az.sql
}

Function Add-ServerFirewallRule
{
    $ip = Invoke-RestMethod -Uri "https://api.ipify.org"
    $firewallRuleName = "ClientIPAddress_" + (Get-Date -Format "yyyy-M-d_H_m_s")

    Write-Host "Add the DB firewall rule ($firewallRuleName) for the current machine (client IP: $ip)"

    New-AzSqlServerFirewallRule `
        -ResourceGroupName blanchirResourceGroup -ServerName blanchirserver `
        -StartIpAddress $ip -EndIpAddress $ip `
        -FirewallRuleName "$firewallRuleName"
}

Login-AzAccount
Import-AzSql
Add-ServerFirewallRule
