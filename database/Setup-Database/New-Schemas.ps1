﻿param(
    [Parameter(Mandatory=$False)]
    [string]
    $serverInstance =
        #'(localdb)\mssqllocaldb' # local
        'blanchirserver.database.windows.net' # azure
    ,
    [Parameter(Mandatory=$False)]
    [string]
    $username =
        #$Null # local
        'blanciradmin' # azure
)

Import-Module -Name 'SQLPS'

$globalVariables = @{
    'dbStructDir' = [System.IO.Path]::GetFullPath(
        [System.IO.Path]::Combine(
            [System.IO.Path]::GetDirectoryName(
                $script:MyInvocation.MyCommand.Path),
            '..\structure\'))
}

Function Invoke-Sql
{
    param(
        [Parameter(Mandatory=$True)]
        [string]
        $fileName
    )
    $inputFile = [System.IO.Path]::Combine(
        $globalVariables['dbStructDir'], "$fileName.sql")
    $scriptPath = Get-ScriptPath
    $scriptBody = Get-ScriptBody `
        -serverInstance $serverInstance `
        -sqlFile $inputFile
    Write-Script -scriptPath $scriptPath -scriptBody $scriptBody
    & $scriptPath
}

Function Get-ScriptPath
{
    $tempPath = [System.IO.Path]::GetTempPath()
    $scriptFileNameWithExt = "Invoke-Sql.ps1"

    [System.IO.Path]::Combine(
        $tempPath, $scriptFileNameWithExt)
}

Function Get-ScriptBody
{
    param(
        [Parameter(Mandatory=$True)]
        [string]
        $serverInstance,
        [Parameter(Mandatory=$True)]
        [string]
        $sqlFile
    )
    $bodyBuilder = New-Object System.Text.StringBuilder
    $bodyBuilder.Append(
        "Invoke-Sqlcmd -ServerInstance '$serverInstance' "
        ) | Out-Null
    $bodyBuilder.Append(
        "-Database 'blanchirDatabase' "
        ) | Out-Null
    $bodyBuilder.Append(
        "-InputFile '$sqlFile' "
        ) | Out-Null
    If ($username -ne $Null)
    {
        $bodyBuilder.Append(
            "-Username '$username' "
            ) | Out-Null
        $passwd = $globalVariables['azSqlDbPasswd']
        $bodyBuilder.Append(
            "-Password '$passwd' "
            ) | Out-Null
    }
    [System.String]::Join(
        [System.Environment]::NewLine,
        $bodyBuilder.ToString())
}

Function Write-Script
{
    param(
        [Parameter(Mandatory=$True)]
        [string]
        $scriptPath,
        [Parameter(Mandatory=$True)]
        [string]
        $scriptBody
    )
    If ([System.IO.File]::Exists($scriptPath))
    {
        [System.IO.File]::Delete($scriptPath)
    }
    Set-Content -Path "$scriptPath" -Value $scriptBody
}

If ($username -ne $Null)
{
    $password = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto(
        [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR(
            (Read-Host -Prompt "Password for the DB" -AsSecureString)))
    $globalVariables.Add('azSqlDbPasswd', $password)
}

Invoke-Sql -fileName 'Administration.SCHEMA'
Invoke-Sql -fileName 'Administration.Users.TABLE'

Invoke-Sql -fileName 'Budgets.SCHEMA'
Invoke-Sql -fileName 'Budgets.Budgets.TABLE'
Invoke-Sql -fileName 'Budgets.BudgetsRows.TABLE'
