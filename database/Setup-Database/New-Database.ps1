param(
    [Parameter(Mandatory=$False)]
    [bool]
    $preserveParams = $False
)

Function New-Database
{
    # read parameters file
    $safeparams = Get-Content -Raw -Path .\template\parameters.json;
    $jsonparams = ConvertFrom-Json -InputObject $safeparams;

    # set dba password
    <# TODO
    $securepasswd = Read-Host -AsSecureString -Prompt "administratorLoginPassword";
    $cryptedpasswd = ConvertFrom-SecureString -SecureString $securepasswd;
    $jsonparams.parameters.administratorLoginPassword = $cryptedpasswd;
    #>
    $passwd = @{};
    $passwd.value = Read-Host -Prompt "administratorLoginPassword";
    $jsonparams.parameters.administratorLoginPassword = $passwd;

    # create temp parameters
    $tempparams = ConvertTo-Json -InputObject $jsonparams
    Set-Content -Path .\parameters.json -Value $tempparams

    # run template deployment
    Try
    {
        .\template\deploy.ps1 `
            -templateFilePath .\template\template.json `
            -parametersFilePath .\parameters.json
    }
    Catch
    {
        Write-Host $_.Exception.Message
        Write-Error $_.ScriptStackTrace
    }

    # remove temp parameters
    If (!$preserveParams)
    {
        Remove-Item .\parameters.json
    }
}

New-Database;
