﻿# run in elevated permissions

Write-Host "Installation of the az.sql module"

$error = $False;

Try
{
    Install-Module az.sql -AllowClobber -ErrorAction Stop -Force;
}
Catch
{
    Write-Error $_;
    $error = $True;
}

Pause;
