# Introduction

## Annotation

For regular users **blanchir** is an Android application aimed to help them
to create, control and manage budgets.

## Architecture

| Layer     | Responsibilities                      | Technology          | IDE            |
|-----------|---------------------------------------|---------------------|----------------|
| Back-end  | Store users' budgets data             | Azure SQL Database  | SSMS           |
| Service   | DAL, BLL. The core of the application | Node.js (express)   | Atom           |
| Front-end | UI. Client for the service            | Android (XML, Java) | Android Studio |

The SSMS stands for SQL Server Management Studio (from Microsoft).

# Back-end

## Full setup of a DB

```PowerShell
Set-Location .\database\
.\Setup-Database.ps1
```

## Deploy the DB to Azure

Information concerning a DB and the according server that will created sit under
_.\database\template\parameters.json_ file. It consists of

* DBA login,
* DB name,
* DB collation,
* and other relevant settings.

You may want to change these settings before continuing.

After parameters are edited, you should run the next commands to deploy
the database template to Azure. Run them every time you want to create a
DB instance of **blanchir**.

```PowerShell
Set-Location .\database\
.\Setup-Database\New-Database.ps1
```

## Create a firewall rule for the server

Run following commands once from each machine you want to connect to the DB.

```PowerShell
Set-Location .\database\
.\Setup-Database\Call-NewServerFirewallRule.ps1
```

This script will ask you for

* subscription ID,
* resource group name and location (for a new RG),
* deployment name,
* authorization.

If you use Atom (just like me), do not try to run these commands from
PowerShell terminals from within the editor: it hopeless in case you want
to copy-paste some values to the deployment script when it prompts.

## Clean up resources

Resources created using this guide will spend your money or credit at Azure.
To prevent this when the database is no longer needed you should remove the
resource group created by a deployment script you used earlier to deploy the
database. Please find the appropriate information concerning
[cleaning up resources at the Azure documentation](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-manager-tutorial-create-encrypted-storage-accounts#clean-up-resources).

# Service

## How to start the service

```PowerShell
Set-Location .\webserver\
npm start
```

## Performing queries to databases

The production database is a Azure SQL Server database.
For development purposes the mimic database has been created (POJO).

To query and modify data using CRUD (4 types of queries) for the Azure SQL and the mimic databases (2 types of databases) I used to use the Bridge, (Simple) Factory and Abstract Factory
software development patterns.

![The draw.io diagram for the Bridge pattern usage to implement two kinds of CRUD operations with the Blanchir data](https://am3pap002files.storage.live.com/y4psLcVVZY3eBrw7NjMhJEQjH8JEH_6lqT0qFIaSERGizMH7sWC60y8iuCFF-3yJ6wPAz2zu3Gc9e_k-pLPFe9IqmHUc4V95IZCWkcz2WRW1-8jDgdYheQ6ymKWKYi_CAdZwmYhSjlkpBvCEDuhQFI-7zPNUSGTE7vL_ExPOSQnexbBcjugRCUiF7F2cxstWotcNMYaZhPQcgBV0TJ5l4rsknebjtoV_yRnmC7eZuXe3Wk/Controllers-SQL%20Bridge.png?psid=1&width=1086&height=610).

The image above may be not actual. Always check the [diagram itself](https://1drv.ms/u/s!AmR1AdzhtSFXiT0RpUwn-Ab3rQYZ?e=17fhc9).
View it using the [draw.io](https://www.draw.io) cloud diagramming tool.

# Front-end

(Nothing is documented for now)
