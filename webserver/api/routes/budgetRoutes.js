'use strict';

module.exports = function(app) {
  const budget = require('../controllers/budgetController'),
    log = require('../controllers/logController');

  // Budgets list
  app.route('/budgets')
    .all(log.log_request)
    .get(budget.list_all_budgets)
    .post(budget.create_a_budget);

  // Budget rows list
  app.route('/budgets/:BudgetID')
    .all(log.log_request)
    .get(budget.list_budget_rows);

  // Budget row
  app.route('/rows')
    .all(log.log_request)
    .post(budget.create_budget_row);
  app.route('/rows/:BudgetRowID')
    .all(log.log_request)
    .delete(budget.delete_budget_row)
    .put(budget.update_budget_row);
};
