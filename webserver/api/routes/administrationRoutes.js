'use strict';

const administration = require('../controllers/administrationController'),
  log = require('../controllers/logController');

module.exports = function(app) {
  app.route('/users')
    .all(log.log_request)
    .post(administration.create_a_user);
};
