'use strict';

// TODO Test if this still works
const bunyan = require('bunyan'),
  log = bunyan.createLogger({
    name: 'blanchir',
    streams: [{
      path: 'log.json'
    }]
  });

module.exports.log_request = function(req, _res, next) {
  log.info(`${req.method} ${req.url}`);
  next();
};
