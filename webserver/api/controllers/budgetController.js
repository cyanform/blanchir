'use strict';

const q = require('../../dbqueries/queries'),
  QueryBridge = require('../../dbqueries/queryBridge'),
  builder = require('../../config').config.queryBuilder;

// TODO Move to budgetsController.js
// TODO Make all queries to the mimic DB async (use error-first callbacks)
exports.list_all_budgets = function(req, res) {
  const query = new q.Select({
    from: 'Budgets.Budgets'
  });

  const bridge = new QueryBridge(query);
  bridge.setBuilder(builder);

  const select = bridge.buildQuery();

  // TODO Such call-backs should be 'error first call-backs' (in factories/)
  const callback = function(budgetsCount, allBudgets) {
    res.json({
      budgets: allBudgets
    });
  }

  select(callback);
}

// TODO Move to budgetsController.js
exports.create_a_budget = function(req, res) {
  const query = new q.Insert({ into: 'Budgets.Budgets', value: req.body });

  // Bridging the instances from two hierarchies
  const bridge = new QueryBridge(query);
  bridge.setBuilder(builder);

  const insert = bridge.buildQuery();

  const callback = function(budgetsCount) {
    // TODO Handle errors
    // TODO Respond the operation status to a caller
    res.end();
  }

  insert(callback);
}

// TODO Move to budgetRowsController.js
exports.list_budget_rows = function(req, res) {
  const query = new q.Select({
    from: 'Budgets.BudgetsRows',
    where: req.params
  });

  const bridge = new QueryBridge(query);
  bridge.setBuilder(builder);

  const select = bridge.buildQuery();

  const callback = function(rowsCount, allRows) {
    return res.json({
      rows: allRows
    });
  }

  select(callback);
}

// TODO Move to budgetRowsController.js
exports.create_budget_row = function(req, res) {
  const query = new q.Insert({ into: 'Budgets.BudgetsRows', value: req.body });

  // Bridging the instances from two hierarchies
  const bridge = new QueryBridge(query);
  bridge.setBuilder(builder);

  const insert = bridge.buildQuery();

  const callback = function(rowsCount) {
    // TODO Handle errors
    // TODO Respond the operation status to a caller
    res.end();
  }

  insert(callback);
}

// TODO Move to budgetRowsController.js
exports.delete_budget_row = function(req, res) {
  const query = new q.Delete({
    from: 'Budgets.BudgetsRows',
    where: req.params
  });

  const bridge = new QueryBridge(query);
  bridge.setBuilder(builder);

  const del = bridge.buildQuery();

  const callback = function(count) {
    // TODO Handle errors
    // TODO Respond the operation status to a caller
    res.end();
  }

  del(callback);
}

// TODO Move to budgetRowsController.js
exports.update_budget_row = function(req, res) {
  // TODO Prevent SQL injections in all the controller requests handlers
  const query = new q.Update({
    table: 'Budgets.BudgetsRows',
    set: req.body,
    where: req.params
  });

  const bridge = new QueryBridge(query);
  bridge.setBuilder(builder);

  const update = bridge.buildQuery();

  const callback = function(count) {
    // TODO Handle errors
    // TODO Respond the operation status to a caller
    res.end();
  }

  update(callback);
}
