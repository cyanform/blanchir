'use strict';

const Insert = require('../../dbqueries/queries').Insert,
  QueryBridge = require('../../dbqueries/queryBridge'),
  builder = require('../../config').config.queryBuilder;

exports.create_a_user = function(req, res) {
  // TODO Debug this query on the mimic DB
  // TODO Rewrite all the queries like the next one and debug them on a mimic DB
  const query = new Insert({ into: 'Administration.Users', value: req.body });

  const bridge = new QueryBridge(query);
  bridge.setBuilder(builder);

  const insertCallback = function() {
    // TODO Make this call back function an error-first callback (in factories/)
    // TODO Handle possible errors
    // TODO Respond the operation status to a caller
    res.end();
  }

  const insert = bridge.buildQuery();
  insert(insertCallback);
}
