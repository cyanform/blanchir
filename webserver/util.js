/**
 * Transform the first letter to lower case.
 */
const camel = function(identifier) {
  return identifier.replace(/./, identifier[0].toLowerCase());
}

module.exports.camel = camel;

