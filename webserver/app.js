const config = require('./config').config;

const port = process.env.BLANCHIR_PORT || 3000,
  app = require('express')(),
  bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const budgetRoutes = require('./api/routes/budgetRoutes'),
  administrationRoutes = require('./api/routes/administrationRoutes');
budgetRoutes(app);
administrationRoutes(app);

app.listen(port);

console.log(
  `Server running at http://127.0.0.1:${port}/. ${config.envReport}`);
