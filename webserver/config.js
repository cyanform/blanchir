const MimicQueryBuilder = require('./dbqueries/builders').MimicQueryBuilder,
  AzSqlQueryBuilder = require('./dbqueries/builders').AzSqlQueryBuilder;

const env = {
  development: 'development',
  production: 'production'
}

const Config = function() {
  const envName = process.env.NODE_ENV || env.development;

  const config = {
    env: envName,
    envReport: `Environment is '${envName}'; ` +
      "use 'set NODE_ENV=development|production' before 'npm start' to change."
  };

  switch (envName) {
    case env.development: {
      // TODO Use Object.assign() here and below to extend `config`
      config.queryBuilder = new MimicQueryBuilder();
      break;
    }
    case env.production: {
      config.queryBuilder = new AzSqlQueryBuilder();
      config.connection = {
        authentication: {
          options: {
            userName: 'blanciradmin',
            password: 'b1anchirPa$$w0rd'
          },
          type: 'default'
        },
        server: 'blanchirserver.database.windows.net',
        options: {
          database: 'blanchirDatabase',
          encrypt: true
        }
      };
      break;
    }
    default: {
      const envKeys = Object.keys(env);
      const envs = [];
      var key;
      for (let k in envKeys) {
        key = envKeys[k];
        envs.push(env[key]);
      }
      envNames = envs.join(', ');
      throw `The '${envName}' isn't recognized environment name. Available:
        ${envNames}`
    }
  }

  return config;
}

const config = new Config();

module.exports.env = env;
module.exports.Config = Config;
module.exports.config = config;
