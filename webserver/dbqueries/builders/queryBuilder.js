// The implementor abstraction <https://en.wikipedia.org/wiki/Bridge_pattern>

const q = require('../../dbqueries/queries');

const QueryBuilder = function() {
  // To be set in derived classes
  // TODO Getter+setter (instanceof QueryAbstractFactory)
  this.queryFactory = null;

  // Builds and returns a function performing a query and fetching results
  this.buildQuery = function(query) {
    if (query instanceof q.Select) {
      return this.queryFactory.makeSelectQuery(query);
    } else if (query instanceof q.Insert) {
      return this.queryFactory.makeInsertQuery(query);
    } else if (query instanceof q.Update) {
      return this.queryFactory.makeUpdateQuery(query);
    } else if (query instanceof q.Delete) {
      return this.queryFactory.makeDeleteQuery(query);
    } else {
      throw `A QueryBuilder.buildQuery(query) instance throws an error:
        unable to determine the type of the passed query: ${query}.`;
    }
  }
}

module.exports = QueryBuilder;
