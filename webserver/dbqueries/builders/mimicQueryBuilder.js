const QueryBuilder = require('./queryBuilder'),
  db = require('.').mimicDB,
  MimicQueryAbstractFactory =
    require('../factories/mimic').MimicQueryAbstractFactory;

// Concrete implementation (https://en.wikipedia.org/wiki/Bridge_pattern)
const MimicQueryBuilder = function() {
  this.queryFactory = new MimicQueryAbstractFactory();
}

MimicQueryBuilder.prototype = new QueryBuilder();
MimicQueryBuilder.prototype.constructor = MimicQueryBuilder;

module.exports = MimicQueryBuilder;
