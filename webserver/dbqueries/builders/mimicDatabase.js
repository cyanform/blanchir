const mimicDB = {

  // SCHEMA Budgets
  budgets: {

    // TABLE Budgets
    budgets: [{
      budgetID: 1,
      userID: 1,
      description: 'Running costs'
    }, {
      budgetID: 2,
      userID: 1,
      description: 'Realty procurements'
    }],

    // TABLE BudgetsRows
    // TOOD Add a field to store planned execution date for budgets rows 1) here and in the 2) Azure SQL database
    budgetsRows: [{
      budgetRowID: 1,
      budgetID: 1,
      completed: 1,
      budgetRowName: 'Dental care'
    }, {
      budgetRowID: 2,
      budgetID: 1,
      completed: 1,
      budgetRowName: 'Charity for July'
    }, {
      budgetRowID: 3,
      budgetID: 1,
      completed: 1,
      budgetRowName: 'Saved money'
    }, {
      budgetRowID: 4,
      budgetID: 1,
      completed: 1,
      budgetRowName: 'Crownwork'
    }, {
      budgetRowID: 5,
      budgetID: 1,
      completed: 1,
      budgetRowName: 'Cash up to 7/06'
    }, {
      budgetRowID: 6,
      budgetID: 1,
      completed: 1,
      budgetRowName: 'DVD burner drive replacement'
    }, {
      budgetRowID: 7,
      budgetID: 1,
      completed: 1,
      budgetRowName: 'Battery replacement for APC Back-UPS CS 350'
    }, {
      budgetRowID: 8,
      budgetID: 1,
      completed: 0,
      budgetRowName: 'Sonic Bomb alarm clock'
    }, {
      budgetRowID: 9,
      budgetID: 1,
      completed: 0,
      budgetRowName: 'Charge for the living room'
    }, {
      budgetRowID: 10,
      budgetID: 1,
      completed: 0,
      budgetRowName: 'Rail service'
    }, {
      budgetRowID: 11,
      budgetID: 1,
      completed: 0,
      budgetRowName: 'Cash up to 7/13'
    }, {
      budgetRowID: 12,
      budgetID: 1,
      completed: 0,
      budgetRowName: 'Therapy (28 left)'
    }, {
      budgetRowID: 13,
      budgetID: 1,
      completed: 0,
      budgetRowName: 'New graphic card for Unit 1'
    }, {
      budgetRowID: 14,
      budgetID: 1,
      completed: 0,
      budgetRowName: 'Cash up to 7/20'
    }, {
      budgetRowID: 15,
      budgetID: 1,
      completed: 1,
      budgetRowName: 'Therapy (27 left)'
    }, {
      budgetRowID: 16,
      budgetID: 2,
      completed: 1,
      budgetRowName: '1MRUR 2019'
    }, {
      budgetRowID: 17,
      budgetID: 2,
      completed: 1,
      budgetRowName: '2MRUR 2020'
    }, {
      budgetRowID: 18,
      budgetID: 2,
      completed: 0,
      budgetRowName: '3MRUR 2021'
      }]

  }

};

module.exports = mimicDB;
