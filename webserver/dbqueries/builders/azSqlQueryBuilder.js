const QueryBuilder = require('./queryBuilder'),
  AzSqlQueryAbstractFactory =
    require('../factories/azSql').AzSqlQueryAbstractFactory;

// Concrete implementation (https://en.wikipedia.org/wiki/Bridge_pattern)
const AzSqlQueryBuilder = function() {
  this.queryFactory = new AzSqlQueryAbstractFactory()
}

AzSqlQueryBuilder.prototype = new QueryBuilder();
AzSqlQueryBuilder.prototype.constructor = AzSqlQueryBuilder;

module.exports = AzSqlQueryBuilder;
