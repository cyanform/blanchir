See the [diagram](https://1drv.ms/u/s!AmR1AdzhtSFXiT0RpUwn-Ab3rQYZ?e=17fhc9)
to learn how the Bridge, the Simple Factory and the Abstract Factory
software design patterns are use to implement CRUD operations for
the cloud production (Azure SQL) and the local development (mimic) databases.
