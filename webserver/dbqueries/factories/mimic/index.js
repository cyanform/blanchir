module.exports.MimicQueryAbstractFactory =
  require('./mimicQueryAbstractFactory');

module.exports.MimicSelectQueryFactory =
  require('./mimicSelectQueryFactory');

module.exports.MimicInsertQueryFactory =
  require('./mimicInsertQueryFactory');

module.exports.MimicUpdateQueryFactory =
  require('./mimicUpdateQueryFactory');

module.exports.MimicDeleteQueryFactory =
  require('./mimicDeleteQueryFactory');
