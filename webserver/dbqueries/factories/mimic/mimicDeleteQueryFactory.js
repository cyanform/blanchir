const q = require('../../queries'),
  getTable = require('./getTable'),
  camel = require.main.require('./util').camel;

const MimicDeleteQueryFactory = function() {
  this.makeDeleteQuery = function(query) {
    if (!(query instanceof q.Delete)) {
      throw `MimicDeleteQueryFactory.makeDeleteQuery(query):
        the 'query' parameter must be an instance of Delete class. Example:
        const Delete = require('.../dbqueries/queries').Delete;
        const MimicDeleteQueryFactory =
          require('.../dbqueries/factories/mimic').MimicDeleteQueryFactory;
        new MimicDeleteQueryFactory().makeDeleteQuery(new Delete({
          from: 'Customer', where: { fullName: 'Doe, John' } }));`;
    }

    const queryFn = function(callback) {
      // FROM
      var table = getTable(query.from);

      // WHERE
      // TODO Make filtering reusable because it's copy-pasted multiple times
      var whereKeys = Object.keys(query.where);
      var row, matched, key;
      var rowsToDelIndexes = [];
      for (let r = table.length - 1; r >= 0; r--) {
        row = table[r]; matched = true;
        for (let k in whereKeys) {
          key = whereKeys[k];
          if (row[camel(key)] != query.where[key]) {
            matched = false;
            break;
          }
        }
        if (matched) {
          rowsToDelIndexes.push(r);
        }
      }

      // DELETE
	  const count = rowsToDelIndexes.length;
      let delRowIndex;
      for (let i = count - 1; i >= 0; i--) {
        delRowIndex = rowsToDelIndexes[i];
        table.splice(delRowIndex, 1);
      }

      callback(count);
    }

    return queryFn;
  }
}

module.exports = MimicDeleteQueryFactory;
