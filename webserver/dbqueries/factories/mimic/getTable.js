const getTable = function(tablePath) {
  // tablePath === '<SCHEMA name>.<TABLE name>'
  // TODO What if tablePath is not well-formed?
  
  // TODO Move out from this function
  const db = require('../../builders').mimicDB;
  
  const path = tablePath.split('.'),
    schemaPathIndex = 0,
    tablePathIndex = 1;
  
  let schemaName = path[schemaPathIndex],
    tableName = path[tablePathIndex];
  
  schemaName = schemaName.replace(/./, schemaName[0].toLowerCase());
  tableName = tableName.replace(/./, tableName[0].toLowerCase());
  
  return db[schemaName][tableName];
}

module.exports = getTable;
