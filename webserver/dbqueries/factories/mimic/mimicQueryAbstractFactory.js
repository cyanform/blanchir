const QueryAbstractFactory = require('../queryAbstractFactory'),
  mimic = require('.');

const MimicQueryAbstractFactory = function() {
  this.selectFactory = null;
  this.insertFactory = null;
  this.updateFactory = null;
  this.deleteFactory = null;

  this.makeSelectQuery = function(query) {
    if (!this.selectFactory) {
      this.selectFactory = new mimic.MimicSelectQueryFactory();
    }

    return this.selectFactory.makeSelectQuery(query);
  }

  this.makeInsertQuery = function(query) {
    if (!this.insertFactory) {
      this.insertFactory = new mimic.MimicInsertQueryFactory();
    }

    return this.insertFactory.makeInsertQuery(query);
  }

  this.makeUpdateQuery = function(query) {
    if (!this.updateFactory) {
      this.updateFactory = new mimic.MimicUpdateQueryFactory();
    }

    return this.updateFactory.makeUpdateQuery(query);
  }

  this.makeDeleteQuery = function(query) {
    if (!this.deleteFactory) {
      this.deleteFactory = new mimic.MimicDeleteQueryFactory();
    }

    return this.deleteFactory.makeDeleteQuery(query);
  }
}

MimicQueryAbstractFactory.prototype = new QueryAbstractFactory();
MimicQueryAbstractFactory.prototype.constructor = MimicQueryAbstractFactory;

module.exports = MimicQueryAbstractFactory;
