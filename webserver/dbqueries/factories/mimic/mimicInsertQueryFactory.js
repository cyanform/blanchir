const q = require('../../queries'),
  getTable = require('./getTable'),
  camel = require.main.require('./util').camel;

const MimicInsertQueryFactory = function() {
  this.makeInsertQuery = function(query) {
    if (!(query instanceof q.Insert)) {
      throw `MimicInsertQueryFactory.makeInsertQuery(query):
        the 'query' parameter must be an instance of Insert class. Example:
        const Insert = require('.../dbqueries/queries').Insert;
        const MimicInsertQueryFactory =
          require('.../dbqueries/factories/mimic').MimicInsertQueryFactory;
        new MimicInsertQueryFactory().makeInsertQuery(new Insert({
          into: 'Customer', value: { fullName: 'Doe, John' } }));`;
    }

    const queryFn = function(callback) {
	  const keys = Object.keys(query.value),
	    value = { };

	  let key;
      for (let k in keys) {
	    key = keys[k];
	    value[camel(key)] = query.value[key];
	  }

      const insertedRowsCount = getTable(query.into).push(value);
      callback(insertedRowsCount);
    }

    return queryFn;
  }
}

module.exports = MimicInsertQueryFactory;
