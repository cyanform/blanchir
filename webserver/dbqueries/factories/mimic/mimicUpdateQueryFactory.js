const q = require('../../queries'),
  getTable = require('./getTable'),
  camel = require.main.require('./util').camel;

const MimicUpdateQueryFactory = function() {
  this.makeUpdateQuery = function(query) {
    if (!(query instanceof q.Update)) {
      throw `MimicUpdateQueryFactory.makeUpdateQuery(query):
        the 'query' parameter must be an instance of Update class. Example:
        const Update = require('.../dbqueries/queries').Update;
        const MimicUpdateQueryFactory =
          require('.../dbqueries/factories/mimic').MimicUpdateQueryFactory;
        new MimicUpdateQueryFactory().makeUpdateQuery(new Update({
          table: 'Customer',
          set: { fullName: 'Doe, John' },
          where: { rowID: 18 }
        }));`;
    }

    const queryFn = function(callback) {
      // UPDATE query.table
      const allRows = getTable(query.table);

      // WHERE
      const whereKeys = Object.keys(query.where);
      var updatingRows;
      if (whereKeys.length) {
        updatingRows = allRows.filter(function(row) {
          var key;
          for (let k in whereKeys) {
            key = whereKeys[k];
            if (row[camel(key)] != query.where[key]) {
              return false;
            }
          }
          return true;
        });
      } else {
        updatingRows = allRows;
      }

      // SET
      const setKeys = Object.keys(query.set);
      var row, key;
      for (let r in updatingRows) {
        row = updatingRows[r];
        for (let k in setKeys) {
          key = setKeys[k];
          row[camel(key)] = query.set[key];
        }
      }

      callback(updatingRows.length);
    }

    return queryFn;
  }
}

module.exports = MimicUpdateQueryFactory;
