const q = require('../../queries'),
      getTable = require('./getTable'),
	  camel = require.main.require('./util').camel;

const MimicSelectQueryFactory = function() {
  this.makeSelectQuery = function(query) {
    if (!(query instanceof q.Select)) {
      throw `MimicSelectQueryFactory.makeSelectQuery(query):
        the 'query' parameter must be an instance of Select class. Example:
        const Select = require('.../dbqueries/queries').Select;
        const MimicSelectQueryFactory =
          require('.../dbqueries/factories/mimic').MimicSelectQueryFactory;
        new MimicSelectQueryFactory().makeSelectQuery(new Select({
          from: 'Customer' }));`;
    }

    const queryFn = function(callback) {
      // SELECT FROM
      const rows = getTable(query.from);

      // WHERE
      const keys = Object.keys(query.where||{});
      let result;
      if (keys.length) {
        result = rows.filter(function(r) {
          let key, currentValue, desiredValue;
          for (let k in keys) {
            key = keys[k];
            currentValue = r[camel(key)];
            desiredValue = query.where[key];
            if (currentValue != desiredValue) {
              return false;
            }
          }
          return true;
        });
      } else {
        result = rows;
      }

      if (!Array.isArray(result)) {
        let item = result;
        result = [];
        if (item) {
          result.push(item);
        }
      }
      callback(result.length, result);
    }

    return queryFn;
  }
}

module.exports = MimicSelectQueryFactory;
