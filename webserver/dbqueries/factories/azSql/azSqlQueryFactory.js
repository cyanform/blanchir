const Connection = require('tedious').Connection,
  Request = require('tedious').Request,
  config = require('../../../config');

// Base class for CRUD queries factories
const AzSqlQueryFactory = function() {
  this.queryDatabase = function(queryText, queryCompletedCallback) {
    const rows = [];

    const queryDatabase = function(connection, queryText) {
      const requestCompletedCallback = function(err, rowCount) {
        if (err) {
          // TODO Log errors using bunyan
          console.error(err);
        } else {
          queryCompletedCallback(rowCount, rows);
        }
      }

      const request = new Request(queryText, requestCompletedCallback);

      const processRow = function(rowObject) {
        const row = {};

        let col;
        for (let c in rowObject) {
          col = rowObject[c];
          row[col.metadata.colName] = col.value;
        }

        return row;
      }

      request.on("row", function(rowObject) {
        const row = processRow(rowObject);

        // TODO config.options.rowCollectionOnRequestCompletion; see the doc:
        // tediousjs.github.io/tedious/api-request.html#function_newRequest
        rows.push(row);
      });

      connection.execSql(request);
    }

    const connection = new Connection(config.config.connection);

    connection.on('connect', function(err) {
      if (err) {
        // TODO Log errors using bunyan
        console.error(err);
      } else {
        queryDatabase(connection, queryText);
      }
    });
  }

  // 'Budgets.Budgets' ~> '[Budgets].[Budgets]'
  this.processTablePath = function(initialPath) {
    // TODO Check initialPath for correctness
    const tablePath = initialPath.split('.').map(function(i) {
      return `[${i}]`;
    }).join('.');

    return tablePath;
  }

  // {BudgetRowID:1} ~> ' WHERE BudgetRowID = 1'
  this.makeWhereClause = function(queryWhere) {
    const whereKeys = Object.keys(queryWhere);
    let where;
    if (whereKeys.length) {
      let key, val;
      const checks = [];
      for (let k in whereKeys) {
        key = whereKeys[k];
        val = queryWhere[key];
        checks.push(`(${key} = ${val})`);
      }
      where = ` WHERE ${checks.join(' AND ')}`;
    } else {
      where = '';
    }

    return where;
  }
}

module.exports = AzSqlQueryFactory;
