const AzSqlQueryFactory = require('.').AzSqlQueryFactory,
  Update = require('../../queries').Update;

const AzSqlUpdateQueryFactory = function() {
  this.makeUpdateQuery = function(query) {
    if (!(query instanceof Update)) {
      throw `AzSqlUpdateQueryFactory.makeUpdateQuery(query):
        the 'query' parameter must be an instance of Update class. Example:
        const Update = require('.../dbqueries/queries').Update;
        const AzSqlUpdateQueryFactory =
          require('.../dbqueries/factories/azSql').AzSqlUpdateQueryFactory;
        new AzSqlUpdateQueryFactory().makeUpdateQuery(new Update({
          table: 'Customer',
          set: { fullName: 'Doe, John' },
          where: { id: 19 }
        }));`;
    }

    const makeQueryText = function() {
      // UPDATE
      const tablePath = factory.processTablePath(query.table);

      // SET
      const setKeys = Object.keys(query.set),
        changes = [];
      let key, val;
      for (let k in setKeys) {
        key = setKeys[k];
        val = query.set[key];
        changes.push(`${key} = N'${val}'`); // TODO Handle other data types
      }
      const changesText = changes.join(', ');

      // WHERE
      const whereText = factory.makeWhereClause(query.where);

      // TODO Check for SQL injections
      const text = `UPDATE ${tablePath} SET ${changesText}${whereText}`;
      return text;
    }

    const factory = this;
    const queryFn = function(updateCallback) {
      factory.queryDatabase(makeQueryText(), updateCallback);
    }

    return queryFn;
  }
}

AzSqlUpdateQueryFactory.prototype = new AzSqlQueryFactory();
AzSqlUpdateQueryFactory.prototype.constructor = AzSqlUpdateQueryFactory;

module.exports = AzSqlUpdateQueryFactory;
