const AzSqlQueryFactory = require('.').AzSqlQueryFactory,
  Delete = require('../../queries').Delete;

const AzSqlDeleteQueryFactory = function() {
  this.makeDeleteQuery = function(query) {
    if (!(query instanceof Delete)) {
      throw `AzSqlDeleteQueryFactory.makeDeleteQuery(query):
        the 'query' parameter must be an instance of Delete class. Example:
        const Delete = require('.../dbqueries/queries').Delete;
        const AzSqlDeleteQueryFactory =
          require('.../dbqueries/factories/azSql').AzSqlDeleteQueryFactory;
        new AzSqlDeleteQueryFactory().makeDeleteQuery(new Delete({
          from: 'Customer',
          where: { fullName: 'Doe, John' }
        }));`;
    }

    const makeQueryText = function() {
      const tablePath = factory.processTablePath(query.from),
        whereText = factory.makeWhereClause(query.where),
        text = `DELETE FROM ${tablePath}${whereText}`;

      return text;
    }

    const factory = this;
    const queryFn = function(deleteCallback) {
      factory.queryDatabase(makeQueryText(), deleteCallback);
    }

    return queryFn;
  }
}

AzSqlDeleteQueryFactory.prototype = new AzSqlQueryFactory();
AzSqlDeleteQueryFactory.prototype.constructor = AzSqlDeleteQueryFactory;

module.exports = AzSqlDeleteQueryFactory;
