const Select = require('../../queries').Select,
  AzSqlQueryFactory = require('.').AzSqlQueryFactory;

const AzSqlSelectQueryFactory = function() {
  this.makeSelectQuery = function(query) {
    const factory = this;

    if (!(query instanceof Select)) {
      throw `AzSqlSelectQueryFactory.makeSelectQuery(query):
        the 'query' parameter must be an instance of Select class. Example:
        const Select = require('.../dbqueries/queries').Select;
        const AzSqlSelectQueryFactory =
          require('.../dbqueries/factories/azSql').AzSqlSelectQueryFactory;
        new AzSqlSelectQueryFactory().makeSelectQuery(new Select({
          from: 'Administration.Users' }));`;
    }

    const makeQueryText = function() {
      const tablePath = factory.processTablePath(query.from),
        where = factory.makeWhereClause(query.where),
        queryText = `SELECT * FROM ${tablePath}${where}`;

      return queryText;
    }

    const queryFn = function(callback) {
      factory.queryDatabase(makeQueryText(), callback);
    }

    return queryFn;
  }
}

AzSqlSelectQueryFactory.prototype = new AzSqlQueryFactory();
AzSqlSelectQueryFactory.prototype.constructor = AzSqlSelectQueryFactory;

module.exports = AzSqlSelectQueryFactory;
