const QueryAbstractFactory = require('../queryAbstractFactory'),
  azSql = require('.');

const AzSqlQueryAbstractFactory = function() {
  this.selectFactory = null;
  this.insertFactory = null;
  this.updateFactory = null;
  this.deleteFactory = null;

  this.makeSelectQuery = function(query) {
    if (!this.selectFactory) {
      this.selectFactory = new azSql.AzSqlSelectQueryFactory();
    }

    return this.selectFactory.makeSelectQuery(query);
  }

  this.makeInsertQuery = function(query) {
    if (!this.insertFactory) {
      // TODO Check if this code called only once per all INSERT queries
      this.insertFactory = new azSql.AzSqlInsertQueryFactory();
    }

    return this.insertFactory.makeInsertQuery(query);
  }

  this.makeUpdateQuery = function(query) {
    if (!this.updateFactory) {
      this.updateFactory = new azSql.AzSqlUpdateQueryFactory();
    }

    return this.updateFactory.makeUpdateQuery(query);
  }

  this.makeDeleteQuery = function(query) {
    if (!this.deleteFactory) {
      this.deleteFactory = new azSql.AzSqlDeleteQueryFactory();
    }

    return this.deleteFactory.makeDeleteQuery(query);
  }
}

AzSqlQueryAbstractFactory.prototype = new QueryAbstractFactory();
AzSqlQueryAbstractFactory.prototype.constructor = AzSqlQueryAbstractFactory;

module.exports = AzSqlQueryAbstractFactory;
