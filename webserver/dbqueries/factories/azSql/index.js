module.exports.AzSqlQueryAbstractFactory =
  require('./azSqlQueryAbstractFactory');

module.exports.AzSqlQueryFactory =
  require('./azSqlQueryFactory');

module.exports.AzSqlInsertQueryFactory =
  require('./azSqlInsertQueryFactory');

module.exports.AzSqlSelectQueryFactory =
  require('./azSqlSelectQueryFactory');

module.exports.AzSqlUpdateQueryFactory =
  require('./azSqlUpdateQueryFactory');

module.exports.AzSqlDeleteQueryFactory =
  require('./azSqlDeleteQueryFactory');
