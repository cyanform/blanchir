const Insert = require('../../queries').Insert,
  Request = require('tedious').Request,
  AzSqlQueryFactory = require('.').AzSqlQueryFactory;

const AzSqlInsertQueryFactory = function() {
  this.makeInsertQuery = function(query) {
    const factory = this;

    if (!(query instanceof Insert)) {
      throw `AzSqlInsertQueryFactory.makeInsertQuery(query):
        the 'query' parameter must be an instance of Insert class. Example:
        const Insert = require('.../dbqueries/queries').Insert;
        const AzSqlInsertQueryFactory =
          require('.../dbqueries/factories/azSql').AzSqlInsertQueryFactory;
        new AzSqlInsertQueryFactory().makeInsertQuery(new Insert({
          into: 'Customer', value: { fullName: 'Doe, John' } }));`;
    }

    const makeQueryText = function() {
      const tablePath = factory.processTablePath(query.into);

      // TODO Check query.value for correctness
      const columns = Object.keys(query.value),
        colNames = columns.join(', ');

      const values = [];
      let key, val;
      for (let k in columns) {
        key = columns[k];

        // TODO Move to convert() outside of this class' hierachy
        // TODO Convert numbers (1->'1') and booleans (to BIT: true->'1')
        val = `N'${query.value[key]}'`;

        values.push(val);
      }
      const colValues = values.join(', ');

      // TODO SQL injections are quite possible!
      const text = `INSERT INTO ${tablePath} (${colNames})
        VALUES (${colValues})`;

      return text;
    }

    const queryFn = function(insertCallback) {
      factory.queryDatabase(makeQueryText(), insertCallback);
    }

    return queryFn;
  }
}

AzSqlInsertQueryFactory.prototype = new AzSqlQueryFactory();
AzSqlInsertQueryFactory.prototype.constructor = AzSqlInsertQueryFactory;

module.exports = AzSqlInsertQueryFactory;
