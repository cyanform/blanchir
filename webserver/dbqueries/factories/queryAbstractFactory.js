const QueryAbstractFactory = function() {
  this.makeSelectQuery = function() {
    throw 'Implement QueryAbstractFactory.makeSelectQuery() in a derived class';
  }

  this.makeInsertQuery = function() {
    throw 'Implement QueryAbstractFactory.makeInsertQuery() in a derived class';
  }

  this.makeUpdateQuery = function() {
    throw 'Implement QueryAbstractFactory.makeUpdateQuery() in a derived class';
  }

  this.makeDeleteQuery = function() {
    throw 'Implement QueryAbstractFactory.makeDeleteQuery() in a derived class';
  }
}

module.exports = QueryAbstractFactory;
