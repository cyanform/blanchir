const Query = require('.').Query;

// Refined abstraction (https://en.wikipedia.org/wiki/Bridge_pattern)
const UpdateQuery = function(u) {
  // Fields
  this.table = null;
  this.set = null;
  this.where = null;

  // Null reference exception prevention
  if (!u) {
    u = { };
  }

  // table_name
  if (typeof u.table !== 'string' || !u.table) {
    throw `In which TABLE would you like to update data? Example:
      const Update = require('.../dbqueries/queries').Update;
      const update = new Update({ table: 'Customers' });`;
  } else {
    this.table = u.table;
  }

  // SET
  if (typeof u.set !== 'object' || !u.set) {
    throw `What values and to which fields do you wish to SET? Example:
      const Update = require('.../dbqueries/queries').Update;
      const update = new Update({ set: { FirstName: 'John' } });`;
  } else {
    this.set = u.set;
  }

  // WHERE
  if (typeof u.where !== 'object' || !u.where) {
    throw `WHERE (which rows) do you wish to change fields values? Example:
      const Update = require('.../dbqueries/queries').Update;
      // UPDATE ... SET ... WHERE ID = 123614:
      const update = new Update({ where: { ID: 123614 } });
      // UPDATE ... SET ... /*all the records*/:
      const updAll = new Update({ where: { } });`;
  } else {
    this.where = u.where;
  }
}

UpdateQuery.prototype = Object.create(Query.prototype);
UpdateQuery.prototype.constructor = UpdateQuery;

module.exports = UpdateQuery;
