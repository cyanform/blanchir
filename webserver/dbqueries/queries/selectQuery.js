const Query = require('.').Query;

// Refined abstraction (https://en.wikipedia.org/wiki/Bridge_pattern)
const SelectQuery = function(s) {
  this.from = null;
  this.where = null;

  // Null reference exception prevention
  if (!s) {
    s = { };
  }

  const getFrom = function(from) {
    if (typeof from !== 'string' || !from) {
      throw `FROM which table would you like to select data? Example:
        const Select = require('.../dbqueries/queries').Select;
        new Select({ from: 'Customers' })`;
    } else {
      return from;
    }
  }

  const getWhere = function(where) {
    if (!where || typeof where !== 'object') {
      return { };
    } else {
      return where;
    }
  }

  this.from = getFrom(s.from); // FROM
  this.where = getWhere(s.where); // WHERE
}

SelectQuery.prototype = Object.create(Query.prototype);
SelectQuery.prototype.constructor = SelectQuery;

module.exports = SelectQuery;
