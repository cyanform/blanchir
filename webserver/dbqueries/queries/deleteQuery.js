const Query = require('.').Query;

// Refined abstraction (https://en.wikipedia.org/wiki/Bridge_pattern)
const DeleteQuery = function(d) {
  // Fields
  this.from = null;
  this.where = null;

  // Null reference exception prevention
  if (!d) {
    d = { };
  }

  // FROM
  if (typeof d.from !== 'string' || !d.from) {
    throw `FROM which table would you like to delete data? Example:
      const Delete = require('.../dbqueries/queries').Delete;
      const del = new Delete({ from: 'Customers' });`;
  } else {
    this.from = d.from;
  }

  // WHERE
  if (typeof d.where !== 'object' || !d.where) {
    throw `Which rows do you want to delete (WHERE). Example:
      const Delete = require('.../dbqueries/queries').Delete;
      // DELETE FROM ... WHERE ID = 6:
      const del = new Delete({ where: { ID: 6 } });
      // DELETE FROM ... /*all records of a table */:
      const delAll = new Delete({ where: { } });`;
  } else {
    this.where = d.where;
  }
}

DeleteQuery.prototype = Object.create(Query.prototype);
DeleteQuery.prototype.constructor = DeleteQuery;

module.exports = DeleteQuery;
