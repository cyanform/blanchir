const Query = require('.').Query;

// Refined abstraction (https://en.wikipedia.org/wiki/Bridge_pattern)
const InsertQuery = function(i) {
  this.into = null;
  this.value = null;

  // Null reference exception prevention
  if (!i) {
    i = { };
  }

  // name of TABLE
  if (typeof i.into !== 'string' || !i.into) {
    throw `INTO which table would you like to insert data? Example:
      const Insert = require('.../dbqueries/queries').Insert;
      new Insert({ into: 'Customers' })`;
  } else {
    this.into = i.into;
  }

  // VALUES
  if (typeof i.value !== 'object' || !i.value) {
    throw `Pass the row VALUE you would like to insert to a DB. Example:
      const Insert = require('.../dbqueries/queries').Insert;
      new Insert({
        into: 'Customers',
        value: {
          firstName: 'John',
          lastName: 'Doe'
        }
      })`;
  } else {
    this.value = i.value;
  }
}

InsertQuery.prototype = Object.create(Query.prototype);
InsertQuery.prototype.constructor = InsertQuery;

module.exports = InsertQuery;
