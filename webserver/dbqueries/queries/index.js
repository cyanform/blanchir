module.exports.Query = require('./Query');
module.exports.Select = require('./selectQuery');
module.exports.Insert = require('./insertQuery');
module.exports.Update = require('./updateQuery');
module.exports.Delete = require('./deleteQuery');
