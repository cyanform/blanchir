// Refined abstraction. See <https://en.wikipedia.org/wiki/Bridge_pattern>

const Query = require('./queries').Query;
const QueryBuilder = require('./builders/queryBuilder');

const QueryBridge = function(query) {
  // Hierarchy 1: queries data
  this.query = null;

  // Hierarchy 2: Implementor aggregation (query builders)
  this.queryBuilder = null;

  if (!query || !(query instanceof Query)) {
    throw `Pass a query to the QueryBridge constructor. Example:
      const QueryBridge = require('.../dbqueries/queryBridge'),
        Insert = require('.../dbqueries/queries').Insert,
        bridge = new QueryBridge(new Insert({ into: 'Customers' }));`;
  } else {
    this.query = query;
  }

  // setImplementor()
  this.setBuilder = function(builder) {
    if (!builder) {
      throw `A not null builder instance must be passed. Example:
        const bridge = require('.../dbqueries/queries').Update;
        const builder = require('.../dbqueries/builders').MimicQueryBuilder;
        bridge.setBuilder(builder);`;
    } else if (!(builder instanceof QueryBuilder)) {
      throw `An object passed to the builder parameter of the setBuilder()
        must be an instance of descendat of the QueryBuilder class.`
    }
    else {
      this.queryBuilder = builder;
    }
  }

  /*
   * Returns a function (f)
   * that performs a SQL query and fetches its result (f.execute())
   */
  this.buildQuery = function() {
      // TODO Implement a QueryBuilder abstraction class (see <https://1drv.ms/u/s!AmR1AdzhtSFXiT0RpUwn-Ab3rQYZ?e=17fhc9>)
      // TODO Check for `instanceof` QueryBuilder
      if (!this.queryBuilder) {
        throw `Call the setBuilder() before calling buildQuery(). Example:
          const Select = require('.../dbqueries/queries').Select;
          const azsql = require('.../dbqueries/queryBuilders').AzSqlQueryBuilder;
          var builder = new Select({ from: 'budgets' });
          builder.setBuilder(azsql);
          const query = builder.buildQuery();
          const allBudgets = query.execute();`;
      }

      return this.queryBuilder.buildQuery(this.query);
    }
}

module.exports = QueryBridge;
